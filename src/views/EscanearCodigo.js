import React, {Fragment, useState} from 'react';
import {
  Text,
  View,
  Dimensions,
  Alert,
  TouchableOpacity,
  ScrollView,
  Modal,
  Image,
  Linking,
  Platform,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import {Component} from 'react/cjs/react.production.min';
import styles from '../assets/scanStyles';
import LinearGradient from 'react-native-linear-gradient';

import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';
import Celda from '../components/Celda';
import CustomAlertComponent from '../components/CustomAlertComponent';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const reg = new RegExp('^[1-7]*$');
    
class EscanearCodigo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qr: '',
      scan: false,
      ScanResult: false,
      result: null,
      visibility: false,
      mensaje: '',
      viewAlert: false,
      titleAlert: '',
    };
    this.cerrarAlert = this.cerrarAlert.bind(this);
  }
 
  onSucces = e => {
    const valor = {qr: e.data};
    this.setState({
      result: e,
      scan: false,
      ScanResult: true,
    });
    const isValor = valor.qr && valor.qr.length === 12;
    this.setState(
      isValor ? valor : this.validacionSerialInterno(),
    );
    if (isValor) this.salirEscaner();
    var data = [...[valor.qr.slice(3,12)][0]];
    if (reg.test(valor.qr.slice(3,12))) {
      /*AQUI AGRUPAMOS LOS CARACTERES PARA SABER CUANTOS HAY POR CADA UNO*/
      let agrupados = {}
      data.forEach( x => {
        if( !agrupados.hasOwnProperty(x)){
          agrupados[x] = 0
        }
        agrupados[x] += 1;
      })
      this.mensajeResultado(agrupados)
    }   
  };
  
  activeQR = () => {
    this.setState({
      scan: true,
    });
    this.setModalVisibility(true);
  };

  scanAgain = () => {
    this.setState({
      scan: true,
      ScanResult: false,
      qr: '',
    });
    this.setModalVisibility(true);
  };

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  mensajeResultado = (data) => {
    if(data.hasOwnProperty("1") && data["1"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de $2'});
    }else if(data.hasOwnProperty("2") && data["2"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de $4'});
    }else if(data.hasOwnProperty("3") && data["3"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de $20'});
    } else if(data.hasOwnProperty("4") && data["4"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de $50'});
    } else if(data.hasOwnProperty("5") && data["5"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de $100'});
      this.setState({isDisabled: !this.state.isDisabled})  
    } else if(data.hasOwnProperty("6") && data["6"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de una Moto'});
        
    }else if(data.hasOwnProperty("7") && data["7"] == 3){
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Felicidades!',mensaje:  'Eres ganador de un Vehículo'});
      
    }else{
      this.setState({viewAlert: !this.state.viewAlert,titleAlert: '¡Lo sentimos!',mensaje:  'No eres ganador'});
        
    }
  };
  
  
  validacionSerialExterno = valorExterno => {
    this.setState({mensaje:  ''});
      Alert.alert(
      'Conejo de tú suerte informa',
      'Por favor contacte a su comprador comercial para validar premio. Su código de validación es:' +
        ' ' +
        valorExterno.qr,
      [
        {
          text: 'OK',
          onPress: () => this.setModalVisibility(!this.state.visibility),
        },
      ],
    );
  };

  validacionSerialInterno = () => {
    this.setState({mensaje:  '', ScanResult: false});
    Alert.alert(
      'Conejo de tú suerte informa',
      'No se pudo leer correctamente el código QR por favor intente nuevamente',
      [
        {
          text: 'OK',
          onPress: () => this.setModalVisibility(!this.state.visibility),
        },
      ],
    );
  };

  salirEscaner = () => {
    this.setState({scan: false});
    this.setModalVisibility(!this.state.visibility);
  };

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  }
  cerrarAlert = () => {
    this.setState({viewAlert: !this.state.viewAlert });
  };

  
  render() {
    const {scan, ScanResult} = this.state;
    const DeviceWidth = Dimensions.get('window').width;
    const tiraDatos = this.state.qr;
    const mensaje = this.state.mensaje;
    var cuadricula = [];
    var arrayCuadricula = [];
    var numSorteo = [];
    const image = require('../assets/fondo-cuadrilla.png');

    // CUADRICULA
    if (reg.test(tiraDatos.slice(3, 12))) {
      this.cuadricula = cuadricula.push(tiraDatos.slice(3, 12));
      arrayCuadricula = [...cuadricula[0]];
      
      // NUMERO SORTEO
      this.numSorteo = numSorteo.push(tiraDatos.slice(0, 3));
    }
    const iconScanColor = '#00000000';

    return (
      <>
        <View>
          <Modal animationType={'slide'} transparent={false} visible={this.state.visibility}>
            <View style={(styles.modalContainer, styles.cardView)}>
              <View>
                {/* SCANER */}
                {scan && (
                  <QRCodeScanner
                    reactivate={true}
                    onRead={this.onSucces}
                    showMarker={true}
                    ref={node => {
                      this.scanner = node;
                    }}
                    cameraStyle={{height: SCREEN_HEIGHT}}
                    customMarker={
                      <View style={styles.rectangleContainer}>
                        <View style={styles.topOverlay}>
                          <Text style={{fontSize: 30, color: 'white', fontWeight: 'bold'}}>
                            Escanear Código QR
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                          <View style={styles.leftAndRightOverlay} />
                          <View style={styles.rectangle}>
                            <Icon name="ios-qr-scanner" size={SCREEN_WIDTH * 0.73} color={iconScanColor} />
                            <Animatable.View style={styles.scanBar} direction="alternate-reverse" iterationCount="infinite" duration={1700} easing="linear" animation={this.makeSlideOutTranslation('translateY', SCREEN_WIDTH * -0.54)} />
                          </View>
                          <View style={styles.leftAndRightOverlay} />
                        </View>
                        <View style={styles.bottomOverlay} />
                      </View>
                    }
                  />
                )}
                <View style={styles.alignBoton}>
                  <TouchableOpacity
                    onPress={this.salirEscaner}
                    style={[styles.buttonTocuh, styles.btnSalir]}>
                    <Text style={styles.buttonTextStyle}>SALIR</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>

        {/* HEADER */}
        <View style={styles.header}>
          <View style={[styles.franja, styles.franja1]}></View>
          <View style={[styles.franja, styles.franja2]}></View>
          <View style={[styles.franja, styles.franja3]}></View>
          <Image style={styles.tinyLogo} source={require('../assets/logo.png')}/>
        </View>

        {/* TEXTO */}
        <View style={{flex: 1}}>
        <LinearGradient
          locations={[0,0.6]}
          colors={['#258dd4', '#FFFFFF']}
          style={styles.linearGradient}>
        
          <ScrollView
            style={styles.scrollViewStyle}
            contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.textTitle1}>Gana al Instante</Text>
              <Text style={styles.textTitle2}>Vehículos 0 Km, Motos y MUCHO dinero</Text>
              
              <View style={styles.borderCuariculas}>
                <View style={styles.border2}>
                  <View style={styles.border3}>
                  {!ScanResult && (
                  <Image source={image}  style={styles.imageBack} resizeMode="cover"></Image>)}
                    {/* INICIO COLUMNA 1 */}
                    <View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[0]} />
                      </View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[3]} />
                      </View>
                      <View style={styles.celda}>
                          <Celda value={arrayCuadricula[6]} />
                      </View>
                    </View>
                    {/* INICIO COLUMNA 2 */}
                    <View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[1]} />
                      </View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[4]} />
                      </View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[7]} />
                      </View>
                    </View>
                    {/* INICIO COLUMNA 3 */}
                    <View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[2]} />
                      </View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[5]} />
                      </View>
                      <View style={styles.celda}>
                        <Celda value={arrayCuadricula[8]} />
                      </View>
                    </View>
                    {/*</ImageBackground>*/}
                  </View>
                </View>
              </View>
            </View>
            
            {ScanResult && mensaje!='' && (
              <View style={styles.alignBoton}>
                <Text style={styles.estiloMensaje}>{mensaje}</Text>
              </View>
            )}

            {/* BOTÓN VALIDAR */}
            {!scan && !ScanResult && (
              <View style={styles.alignBoton}>
                <TouchableOpacity
                  onPress={this.activeQR}
                  style={[styles.buttonTocuh, styles.buttonTouchable]}>
                  <Text style={styles.buttonTextStyle}>VALIDAR</Text>
                </TouchableOpacity>
              </View>
            )}
            {ScanResult && (
              <Fragment>
                <View style={styles.viewBtnValidarNuevo}>
                  <TouchableOpacity
                    onPress={this.scanAgain}
                    style={[styles.buttonTocuh, styles.buttonTouchable2]}>
                    <Text style={styles.buttonTextStyle}>
                      VALIDAR DE NUEVO
                    </Text>
                  </TouchableOpacity>
                </View>
              </Fragment>
            )}
            {/* </ImageBackground> */}
          </ScrollView>
          </LinearGradient>
        </View>
        {/* FOOTER */}
        <View style={styles.footer}>
          <View style={styles.borderFooter}>
            <View style={{flexDirection: 'row'}}>
            <View>
                <View style={styles.viewFooterLogo}>
                  <Image style={styles.logoLoteria} source={require('../assets/loteria.png')} />
                </View>
              </View>
              <View>
                <View style={styles.viewFooter}>
                  <TouchableOpacity onPress={() =>
                      Linking.openURL(
                        'https://twitter.com/lotdeltachira?lang=es',
                      )
                    }>
                    <Image style={styles.tinyLogoRedes} source={require('../assets/twitter.png')} />
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <View style={styles.viewFooter}>
                  <TouchableOpacity onPress={() =>
                      Linking.openURL(
                        'https://instagram.com/lotdeltachira',
                      )
                    }>
                    <Image style={styles.tinyLogoRedes} source={require('../assets/instagram.png')} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
        { this.state.viewAlert && (
        <View style={styles.container} visible={this.state.viewAlert}>
          <CustomAlertComponent
            displayAlert={true}
            alertTitleText={this.state.titleAlert}
            alertMessageText={this.state.mensaje}
            positiveButtonText={'OK'}
            onPressPositiveButton={this.cerrarAlert}
          />
        </View>)}
      
      </>
    );
  }
}

export default EscanearCodigo;