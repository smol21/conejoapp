import { Dimensions, Platform } from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const DeviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
// CONSTANTES PARA EL ESCANER
const SCREEN_WIDTH = Dimensions.get("window").width;
const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "#fff";

const scanBarWidth = SCREEN_WIDTH * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "rgb(117,209,250)";  

const imageWidth = deviceWidth*0.90;
const imageHeight = Math.round(imageWidth/1.759777);
const left = deviceWidth * 0.05

const styles = {
  franja:{
    position: 'absolute',
    transform: [{ rotate: '177deg'}],
    zIndex: 0,
  },
  franja1:{
    height: (imageHeight*0.45) + 7,
    top: -45,
    backgroundColor: '#074bac',
    width: deviceWidth + 100,
  },
  franja2:{
    top: ((imageHeight*0.45) + 7)-40,
    backgroundColor: '#011689',
    width: deviceWidth + 100,
    height: (imageHeight*0.80) + 10,
  },
  franja3:{
    top: (((imageHeight*0.5) + 7)  -50) + ((imageHeight*0.72) + 7) + 5,
    backgroundColor: '#258dd4',
    width: deviceWidth + 100,
    height: (imageHeight*0.5) + 7,
  },
  cardView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  
  imageBack:{
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: DeviceWidth * 0.78,
    height: DeviceWidth * 0.51,
  },

  // BASE
  scrollViewStyle: {
    flex: 1,
  },
  // Headers
  header: {
    backgroundColor: 'rgb(1,22,137)',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 10,
  },
  linearGradient:{
    height: deviceHeight - (imageHeight + 20 ) - 50
  },
  tinyLogo: {
    width: imageWidth,
    height: imageHeight,
    resizeMode: 'cover',
  },
  tinyLogoRedes:{
    width: 30,
    height: 30,
  },
  logoLoteria: {
    position: 'absolute',
    width: 65,
    height: 65,
    top: -35,
    bottom: 0,
  },
  // Seccion texto
  textTitle1: {
    fontSize: 35,
    textAlign: 'center',
    fontFamily: Platform.OS === "android" ? 'grobold' : 'GROBOLD',
    color: '#fff42b',
    textShadowColor: '#000000',
    textShadowOffset: { width: 3, height: 3 },
    textShadowRadius: 1,
  },
  textTitle2: {
    fontSize: 18,
    textAlign: 'center',
    color: '#FFF',
    marginTop: 5,
    fontFamily: Platform.OS === "android" ? 'fradm' : "FranklinGothic-Demi",
  },

  //Cuadricula
  borderCuariculas: {
    flexDirection: 'row',
    marginTop: 20,
    borderColor: '#78b6e5',
    borderWidth: 4,
    borderRadius: 49,
    position: 'relative',
    
  },
  border2:{
    borderColor: '#258dd4',
    borderWidth: 5,
    borderRadius: 45,
  },
  border3:{
    flexDirection: 'row',
    borderColor: '#011689',
    borderWidth: 8,
    borderRadius: 40,
    backgroundColor: '#fffcc9',
    zIndex: 10,
    overflow: 'hidden',
    
  },

  estiloMensaje: {
    marginTop: 20,
    fontSize: 20,
    color: '#011689',
    textAlign: 'center',
    fontFamily: Platform.OS === "android" ? 'framd' : 'FranklinGothic-Medium',
  },

  celda:{
    width: DeviceWidth * 0.26,
    height: DeviceWidth * 0.17,
    backgroundColor: 'transparent',
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
 
  // BOTON
  alignBoton: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: Platform.OS === "android" ? 0 : 30,
  },
  viewBtnValidarNuevo: {
    alignSelf: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonTocuh: {
    backgroundColor: '#011689',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius:50,
    
  },
  buttonTouchable: {
    fontSize: 25,
    marginBottom: Platform.OS === "android" ? 15 : 0,
    backgroundColor: '#011689',
    width: deviceWidth - 200,
    marginTop: 60
  },
  buttonTouchable2: {
    fontSize: 25,
    marginBottom: Platform.OS === "android" ? 15 : 0,
    width: 260,
    marginTop: 20
  },
  btnSalir:{
    fontSize: 25,
    marginBottom: 30,
    width: deviceWidth - 200,
  },

  buttonTextStyle: {
    color: '#fff42b',
    //fontWeight: 'bold',
    fontFamily: Platform.OS === "android" ? 'gotham' : 'Gotham-Bold',
    fontSize: 20,
  },

  //footer
  footer:{
    backgroundColor: '#011689',
    alignItems: 'center',
    height:50

  },
  borderFooter:{
    justifyContent:"center",
    alignItems:"center",
    paddingTop:10,
  },
  viewFooterLogo:{
    width: DeviceWidth - (120 + 50),
    flex: 1,
  },
  viewFooter: {
    width: 60,
    height: DeviceWidth * 0.15,
    marginBottom: 1,
    flex: 1,
    alignItems: 'flex-end',
    alignSelf: 'center',
    justifyContent: 'center',
  },

  // ESTILOS PARA EL ESCANER
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    position: 'absolute',
  },
  mainOuterComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  mainContainer: {
    //flexDirection: 'column',
    height: 200,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
  },
  topPart: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    paddingTop: 4
  },
  middlePart: {
    position: 'relative',
    flex: 1,
    //width: '100%',
    color: '#FFFFFF',
    fontSize: 16,
    paddingTop: 0,
  },
  bottomPart: {
    width: '100%',
    flexDirection: 'row-reverse',
    padding: 4,
  },
  alertTitleTextStyle: {
    flex: 1,
    textAlign: 'center',
    color: "#011689",
    fontSize: 30,
    paddingTop: 2,
    marginHorizontal: 2,
    marginTop: 5,
    fontFamily: Platform.OS === "android" ? 'fradmcn' : 'FranklinGothic-DemiCond'
  },
  alertMessageTextStyle: {
    color: '#000000',
    textAlign: 'center',
    fontSize: 25,
    paddingHorizontal: 20,
    fontFamily: Platform.OS === "android" ? 'fradmcn' : 'FranklinGothic-DemiCond'
  },
  alertMessageButtonStyle: {
    width: 40,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertMessageButtonTextStyle: {
    fontSize: 20,
    color: '#018b38',
    fontFamily: Platform.OS === "android" ? 'fradmcn' : 'FranklinGothic-DemiCond'
  },
};
export default styles;