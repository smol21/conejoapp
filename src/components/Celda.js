import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import styles from '../assets/scanStyles';
import _interopRequireDefault from '@babel/runtime/helpers/interopRequireDefault';

const Celda = (props) => {
  return (
    <View>
      {props.value === "1" && (
          <Image source={require('../assets/1.png')} />
      )}
      {props.value === "2" && (
          <Image source={require('../assets/2.png')} />
      )}
      {props.value === "3" && (
          <Image source={require('../assets/3.png')} />
      )}
      {props.value === "4" && (
          <Image source={require('../assets/4.png')} />
      )}
      {props.value === "5" && (
          <Image source={require('../assets/5.png')} />
      )}
      {props.value === "6" && (
          <Image source={require('../assets/6.png')} />
      )}
      {props.value === "7" && (
          <Image source={require('../assets/7.png')}/>
      )}
    </View>
  )
}

export default Celda
