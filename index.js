/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import EscanearCodigo from './src/views/EscanearCodigo';


AppRegistry.registerComponent(appName, () => EscanearCodigo);
