## Instalacion de dependencias
`npm install`  

## Correr servidor react-native
`react-native start`

## Correr aplicacion para android
Se debe tener corriendo un emulador de android studio o un telefono conectado en `modo desarrollador`   
abrir otra consola y correr el siguiente comando: 
`react-native run-android --verbose`

## Generar apk
Si es primera vez se debe correr el comando: `react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res`

Correr comandos:  
- `cd android`  
- `gradlew assembleDebug`